BonusType BT4{
	Type=ItemBind
	Duration=Discrete/Continuous/Independent (By Default the behavior of this attribute is to be independent)
}

// The levels are inclusive
Criteria C4{
	Item=ItemId
}

Item health{
	Health=ItemId
}

Item battlepoints{
	Experience=ItemId
}

ItemList IL{
	Assign=RANDOM
	Item=health,battlepoints
}

Bonus B3{
	BonusType=BT4
	Criteria=C4
	ItemList=IL
}